//$(document).ready(function()
//{

/* Twitter */

/* Twitter #1 */

jQuery(function($){
   $("#feptweet").tweet({
      username: "flatearthprod",
      join_text: "auto",
      avatar_size: 0,
      count: 5,
      auto_join_text_default: "we said,",
      auto_join_text_ed: "we",
      auto_join_text_ing: "we were",
      auto_join_text_reply: "we replied to",
      auto_join_text_url: "we were checking out",
      loading_text: "loading tweets...",
      template: "{text}"
   });
}); 

/* Twitter #2 */

jQuery(function($){
   $("#thrtweet").tweet({
      username: "thr",
      join_text: "auto",
      avatar_size: 0,
      count: 5,
      auto_join_text_default: "we said,",
      auto_join_text_ed: "we",
      auto_join_text_ing: "we were",
      auto_join_text_reply: "we replied to",
      auto_join_text_url: "we were checking out",
      loading_text: "loading tweets...",
      template: "{text}"
   });
}); 

/* Twitter #3 */

jQuery(function($){
   $("#fsrtweet").tweet({
      username: "rejectnation",
      join_text: "auto",
      avatar_size: 0,
      count: 5,
      auto_join_text_default: "we said,",
      auto_join_text_ed: "we",
      auto_join_text_ing: "we were",
      auto_join_text_reply: "we replied to",
      auto_join_text_url: "we were checking out",
      loading_text: "loading tweets...",
      template: "{text}"
   });
}); 

/* Support list */

    $("#slist a").click(function(e){
      e.preventDefault();
      $(this).next('p').toggle(200);
    });

/* Portfolio */

// filter items when filter link is clicked
    $('#filters a').click(function(){
      var selector = $(this).attr('data-filter');
      $container.isotope({ filter: selector });
      return false;
    });


/*    $(".prettyphoto").prettyPhoto({
      overlay_gallery: false, social_tools: false
    });
*/

/* email obfuscation */

  $('#my-email').html(function(){
    var e = "kevinoneill";
    var a = "@";
    var d = "flatearthproductions";
    var c = ".com";
    var h = 'mailto:' + e + a + d + c;
    $(this).parent('a').attr('href', h);
    return e + a + d + c;
  });

//qtip2
$(document).ready(function()
{
	// Match all <A/> links with a title tag and use it as the content (default).
	$('a[title]').qtip({
    style: {
      classes: 'qtip-rounded qtip-dark'
    }
  });
});

/* Flexslider */
  $('#projectSlider').flexslider({
    animation: "slide",
    controlsContainer: ".flex-nav-container",
    randomize: "true",
    pauseOnHover:"true"
  });

