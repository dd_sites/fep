/* jQuery Web Site Scripts goes here */
jQuery(document).ready(function() {
	
	/* Enable Back To Top Link */	
	$(window).scroll(function(){
	        // global scroll to top button
	        if ($(this).scrollTop() > 300) {
	            $('.scrolltotop').fadeIn();
	        } else {
	            $('.scrolltotop').fadeOut();
	        }        
	});
	$('.scrolltotop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 700);
        return false;
    });
   


	/* Enable touchTouch for image popup only if present on the page */
	if($('ul.thumbnails li a').length > 0) {
		$('ul.thumbnails li a').touchTouch();
	}



	/* Overlay and Zoom icon */
	$(".zoom").each(function () {
		var d = $(this);
		var b = d.find("img").height();
		var c = $("<span>").addClass("zoom-overlay").html("&nbsp;");
		d.append(c)
	});



	/* Enable Bootstrap Tooltip */
	$('a.tip').tooltip({'placement' : 'bottom'}); 


	/* Enable Bootstrap Tabs */
	$('#tabs a').click(function (e) {
  		e.preventDefault();
  		$(this).tab('show');
	});




	/* Isotope */
	$(window).load(function() {
		var c = $("#portfolio-items");
		function b(d) {
			c.isotope({
				filter: d
			});
			$("#portfolio li.active").removeClass("active");
			$("#portfolio-filter").find("[data-filter='" + d + "']").parent().addClass("active");
			if (d != "*") {
				window.location.hash = d.replace(".", "")
			}
			if (d == "*") {
				window.location.hash = ""
			}
		}
		if (c.length) {
			$(".project").each(function () {
				$this = $(this);
				var d = $this.data("tags");
				if (d) {
					var f = d.split(",");
					for (var e = f.length - 1; e >= 0; e--) {
						$this.addClass(f[e])
					}
				}
			});
			
			c.isotope({
			itemSelector: ".project",
			layoutMode: "fitRows"
			});

			$("#portfolio-filter li a").click(function () {
				var d = $(this).attr("data-filter");
				b(d);
				return false
			});
			if (window.location.hash != "") {
				b("." + window.location.hash.replace("#", ""))
			}
		}
	});


	// Validate
	// http://bassistance.de/jquery-plugins/jquery-plugin-validation/
	// http://docs.jquery.com/Plugins/Validation/
	// http://docs.jquery.com/Plugins/Validation/validate#toptions
	
	$('#contact-form').validate({
		rules: {
			name: {
				minlength: 2,
				required: true
			},
			email: {
				required: true,
				email: true
			},
			message: {
				minlength: 2,
				required: true
			}
		},
		highlight: function(label) {
			$(label).closest('.control-group').addClass('error');
		},
		success: function(label) {
			label
			.text('OK!').addClass('valid')
			.closest('.control-group').addClass('success');
		}
	});
	  

});
